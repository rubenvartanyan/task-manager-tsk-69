package ru.vartanyan.tm.exception;

public class NoSuchUserException extends AbstractException {

    public NoSuchUserException() {
        super("Error! No such uer found...");
    }

}
