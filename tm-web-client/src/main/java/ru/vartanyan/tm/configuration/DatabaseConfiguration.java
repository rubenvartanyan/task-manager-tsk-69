package ru.vartanyan.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.vartanyan.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@ComponentScan("ru.vartanyan.tm")
@EnableJpaRepositories("ru.vartanyan.tm.repository")
@EnableTransactionManagement(proxyTargetClass = true)
public class DatabaseConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Bean
    public DataSource dataSource(
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        @Nullable final String driver = propertyService.getJdbcDriver();
        @Nullable final String username = propertyService.getJdbcUser();
        @Nullable final String password = propertyService.getJdbcPassword();
        @Nullable final String url = propertyService.getJdbcUrl();

        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.vartanyan.tm.model", "ru.vartanyan.tm.dto");

        @NotNull final Properties properties = new Properties();
        @Nullable final String dialect = propertyService.getHibernateDialect();
        @Nullable final String auto = propertyService.getHibernateBM2DDLAuto();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        @Nullable final String secondLevelCash = propertyService.getSecondLevelCash();
        @Nullable final String queryCache = propertyService.getQueryCache();
        @Nullable final String minimalPuts = propertyService.getMinimalPuts();
        @Nullable final String regionPrefix = propertyService.getRegionPrefix();
        @Nullable final String cacheProvider = propertyService.getCacheProvider();
        @Nullable final String factoryClass = propertyService.getFactoryClass();
        @Nullable final String liteMember = propertyService.getLiteMember();

        properties.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        properties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        properties.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);
        if ("true".equals(secondLevelCash)) {
            properties.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, secondLevelCash);
            properties.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, queryCache);
            properties.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, minimalPuts);
            properties.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, regionPrefix);
            properties.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, cacheProvider);
            properties.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, factoryClass);
            properties.put("hibernate.cache.hazelcast.use_lite_member", liteMember);
        }

        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
