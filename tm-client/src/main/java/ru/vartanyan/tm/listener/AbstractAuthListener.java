package ru.vartanyan.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.AdminEndpoint;
import ru.vartanyan.tm.endpoint.SessionEndpoint;

public abstract class AbstractAuthListener extends AbstractListener {

    @NotNull
    @Autowired
    public SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    public AdminEndpoint adminEndpoint;

}
