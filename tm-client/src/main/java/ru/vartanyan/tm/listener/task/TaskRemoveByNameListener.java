package ru.vartanyan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractTaskListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class TaskRemoveByNameListener extends AbstractTaskListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER NAME");
        @NotNull final String name = TerminalUtil.nextLine();
        taskEndpoint.removeTaskByName(name, session);
        System.out.println("[TASK REMOVED]");
    }

}
